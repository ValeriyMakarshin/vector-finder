import argparse

from src.duplicate_finder import find_duplicate_for_file, find_duplicate

DIRECTORY_PATH_KEY = 'directory_path'


def print_duplicate_vector_items(duplicate_vector_items: list):
    for group in duplicate_vector_items:
        for item in group:
            print(item)
        print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(DIRECTORY_PATH_KEY, help='директория проекта')
    parser.add_argument('--find', action='store', help='файл, который будет учавствовать в поиске')
    args = parser.parse_args()

    directory_path = args.__getattribute__(DIRECTORY_PATH_KEY)
    file_for_search = args.__getattribute__('find')

    if file_for_search:
        result_search = find_duplicate_for_file(directory_path, file_for_search)
        print_duplicate_vector_items([result_search])
    else:
        duplicated_items = find_duplicate(directory_path)
        print_duplicate_vector_items(duplicated_items)
