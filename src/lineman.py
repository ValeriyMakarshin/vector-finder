import os
import re

PATH_NECESSARY_FOLDERS_REGEX = r'^(?=^(?:(?!build).)*$)(?=.*drawable$).*$'
XML_FILE_EXTENSION = '.xml'


def get_drawable_path_items(path: str) -> list:
    drawable_paths = list()
    for root, directories, files in os.walk(path):
        if re.match(PATH_NECESSARY_FOLDERS_REGEX, root):
            drawable_paths += list(os.path.join(root, file) for file in files if XML_FILE_EXTENSION in file)
    return drawable_paths
