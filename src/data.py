class Vector:
    def __init__(self, vector_path: str, height: str, width: str, paths: set):
        self.vector_path = vector_path
        self.height = height
        self.width = width
        self.paths = paths

        sorted_paths = self.get_sorted_paths()
        formatted_paths = ' '.join(str(i) for i in sorted_paths)
        self.key_for_sort = self.height + self.width + formatted_paths

    def get_sorted_paths(self) -> list:
        path_list = list(self.paths)
        path_list.sort(key=(lambda x: str(x)))
        return path_list

    def __str__(self) -> str:
        return 'Vector(path = {})'.format(self.vector_path)


class Path:
    def __init__(self, fill_color, path_data, stroke_color):
        self.fill_color = fill_color
        self.path_data = path_data
        self.stroke_color = stroke_color
        self.cache_str = 'Path(fill_color = {}, path_data = {}, stroke_color = {})'.format(
            self.fill_color,
            self.path_data,
            self.stroke_color
        )

    def __str__(self):
        return self.cache_str
