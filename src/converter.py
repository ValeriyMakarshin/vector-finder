from typing import Optional
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from src.data import Vector, Path

ANDROID_URL_MASK = '{http://schemas.android.com/apk/res/android}'


def process_xml(file_path: str) -> Optional[Vector]:
    root = ElementTree.parse(file_path).getroot()
    if root.tag == 'vector':
        height = root.attrib[ANDROID_URL_MASK + 'height']
        width = root.attrib[ANDROID_URL_MASK + 'width']
        paths = process_paths(root)
        vector = Vector(file_path, height, width, paths)
        return vector
    else:
        return None


def process_paths(root: Element) -> set:
    path_elements = root.findall('path')
    paths = set()
    for path in path_elements:
        keys = path.attrib.keys()
        fill_color_key = ANDROID_URL_MASK + 'fillColor'
        path_data_key = ANDROID_URL_MASK + 'pathData'
        stroke_color_key = ANDROID_URL_MASK + 'strokeColor'
        fill_color = path.attrib[fill_color_key] if fill_color_key in keys else 'None'
        path_data = path.attrib[path_data_key] if path_data_key in keys else 'None'
        stroke_color = path.attrib[stroke_color_key] if stroke_color_key in keys else 'None'
        paths.add(Path(fill_color, path_data, stroke_color))
    return paths


if __name__ == '__main__':
    result = process_xml("/Users/v.makarshin/Projects/StudioProjects/openbank-msb-android/core/src/main/res/drawable"
                         "/icon_filter_zero.xml")
    print(result)
