from src.converter import process_xml
from src.lineman import get_drawable_path_items


def get_grouped_items(items: list) -> list:
    values = set(map(lambda item: item.key_for_sort, items))
    return [[item for item in items if item.key_for_sort == value] for value in values]


def get_vector_items_in_directory(directory_path: str) -> list:
    xml_items = get_drawable_path_items(directory_path)
    option_vector_items = list(process_xml(item) for item in xml_items)
    return list(item for item in option_vector_items if item)


def get_duplicate_vector_items(vector_items: list, min_repetitions_count: int = 2) -> list:
    grouped_items = get_grouped_items(vector_items)
    duplicate_items = list(item for item in grouped_items if len(item) >= min_repetitions_count)
    duplicate_items.sort(key=lambda item: len(item))
    return duplicate_items


def find_duplicate(start_directory_path: str) -> list:
    vector_items_in_directory = get_vector_items_in_directory(start_directory_path)
    return get_duplicate_vector_items(vector_items_in_directory)


def find_duplicate_for_file(start_directory_path: str, file_path_for_comparison: str) -> list:
    file_for_comparison = process_xml(file_path_for_comparison)
    file_for_comparison_key = file_for_comparison.key_for_sort

    vector_items_in_directory = get_vector_items_in_directory(start_directory_path)
    list_for_search = get_duplicate_vector_items(vector_items_in_directory, 1)

    return next(group for group in list_for_search if file_for_comparison_key == group[0].key_for_sort)
